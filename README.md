# Zadanie: Samochodowy licznik prędkości średniej

Twoim zadaniem jest napisanie implementacji klasy **Car**, która będzie wyliczała prędkość średnią
pojazdu w oparciu o dane pochodzące z czujnika prędkości chwilowej.

Dane z czujnika prędkości możesz pobrać odwołując się do adresu http://danhoss.vdl.pl/get-velocity.php
przekazując GETem zmienną _t_ o wartości odpowiadającej liczbie sekund, które minęły od początku jazdy.
Projektując klasę pamiętaj o tym, że dane z czujnika będą spływały w trakcie całej podróży, więc nie wiadomo początkowo ile ich będzie (długość podróży jest nieznana i może wynieść nawet kilkadziesiąt godzin). Uwzględnij też to, że nie zawsze będzie można odpytać czujnik ze stałym interwałem (np. co 1s), bo np. komputer pokładowy będzie zajęty manewrem zawracania korytarzem życia na autostradzie po wypadku (co ma większy priorytet). Możesz zasymulować to pobierając dane z czujnika co `1 + rand(0,5)` sekund.

Sugestia:
> Nie chodzi nam o rozwiązanie, które będzie podawało wynik z dokładnością do ósmego miejsca po przecinku,
> ale o rozwiązanie, które nie będzie zwiększało swoich wymagań pamięciowych drastycznie wraz ze wzrostem ilości danych (algorytm in situ).
> Innymi slowy, przechowywanie wszytskich próbek w tablicy i zrobienie na końcu `array_sum($samples)/count($samples)`
> odpada.

Wykorzystując napisaną klasę policz średnią prędkość dla podróży zaczynającej się w czasie _t = 0_ i trwającej 14m 2s z krokiem _ㅿt = 1s_  