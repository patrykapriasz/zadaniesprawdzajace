<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h3>ŚREDNIA PRĘDKOŚĆ AUTA</h3>
    <form action="index.php" method="post">
        <label>Wyznacz czas podróży</label>
        <br>
        <input type ="number" name="hours" min="0"/>HH
        <input type = "number" name="minutes" min="0" max="59">MM
        <input type = "number" name = "seconds" min="0" max="59">SS
        <br>
        <br>
        <input type="submit" value="Rozpocznij podróż"/>
    </form>

</body>
</html>

<?php
require('Car.php');

$driveTime = 0;

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    if(isset($_POST['hours']) && $_POST['hours'] != 0){
        $driveTime = $_POST['hours'] * 3600;
    }
    if(isset($_POST['minutes']) && $_POST['minutes'] != 0){
        $driveTime += $_POST['minutes'] * 60;
    }
    if(isset($_POST['seconds']) && $_POST['seconds'] != 0){
        $driveTime += $_POST['seconds'];
    }
}

if($driveTime != 0){
    $myCar = new Car();
    $myCar -> setDriveTime($driveTime);
    $myCar -> startDrive();
} 
?>