<?php
class Car 
{

    private $sumSpeed = 0;
    private $avgSpeed;
    private $driveTime;
    private $myUrl = "http://danhoss.vdl.pl/get-velocity.php?t=";
    
    public function __constructor(){
    }

    public function setDriveTime($dtime){
        $this->driveTime = $dtime;
    }

    public function getAvgSpeed(){
        return $this->avgSpeed;
    }

    public function connectToSensor($time){
        try {
            $context = file_get_contents( $this->myUrl . $time);
            return $context;
        } catch (\Throwable $th) {
            echo 'Błąd połączenia z czujnikiem';
        }
    }

    public function startDrive(){

        $currentTime = 1;
        $iterrator=0;        
        $shouldWork = true;

        while($shouldWork){
            //przedział czasowy od 1, aby nie wylosowało zera i dwa razy nie odczytało tej samej wartości
            $currentTime+=rand(1,10);
            $this->sumSpeed += $this->connectToSensor($currentTime);
            
            if($currentTime>$this->driveTime){
                $shouldWork = false;
                $this->avgSpeed = $this->sumSpeed / $iterrator;
                $time = gmdate("H:i:s",$this->driveTime);
                $this->avgSpeed = number_format($this->avgSpeed,2);
                echo "<h2>Twoja średnia prędkość w czasie $time to $this->avgSpeed KM/h<h2>";
            }
            $iterrator++;
        }
    }
}
    
?>